#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QRasterWindow>
#include <QTimer>

namespace LayerShellQt
{
class Window;
}

class MainWindow : public QRasterWindow
{
    Q_OBJECT

public:
    MainWindow();
    ~MainWindow();

    void positionPanel();

protected:
    void showEvent(QShowEvent *event) override;
    void paintEvent(QPaintEvent *event) override;

private:
    LayerShellQt::Window *m_layerWindow = nullptr;
    QTimer m_showTimer;
    QTimer m_badResizeTimer;
};
#endif // MAINWINDOW_H
