#include "mainwindow.h"

#include <LayerShellQt/Window>
#include <QPainter>
#include <QDebug>


MainWindow::MainWindow()
    : QRasterWindow()
{
    m_showTimer.setSingleShot(true);
    m_showTimer.setInterval(1000);
    connect(&m_showTimer, &QTimer::timeout, this, [this] () {
        m_badResizeTimer.stop();
        show();
        resize(233, 36);
        qWarning()<<"show and proper resize to 233,26";
    });
    m_showTimer.start();

    m_badResizeTimer.setSingleShot(true);
    m_badResizeTimer.setInterval(50);
    connect(&m_badResizeTimer, &QTimer::timeout, this, [this] () {
        qWarning()<<"wrong resize to 36,36";
        resize(36,36);
    });
    m_badResizeTimer.start();

    m_layerWindow = LayerShellQt::Window::get(this);
    m_layerWindow->setLayer(LayerShellQt::Window::LayerTop);
    m_layerWindow->setKeyboardInteractivity(LayerShellQt::Window::KeyboardInteractivityNone);
    m_layerWindow->setScope(QStringLiteral("dock"));
    m_layerWindow->setCloseOnDismissed(false);
}

MainWindow::~MainWindow()
{
}

void MainWindow::positionPanel()

{
    QMargins margins;
    LayerShellQt::Window::Anchors anchors;
    LayerShellQt::Window::Anchor edge;

    anchors.setFlag(LayerShellQt::Window::AnchorTop);
    margins.setTop(0);
    edge = LayerShellQt::Window::AnchorTop;

    m_layerWindow->setAnchors(anchors);
    m_layerWindow->setExclusiveEdge(edge);
    m_layerWindow->setMargins(margins);
    resize(233, 36);
    //qWarning()<<"PROPER RESIZE";
}

void MainWindow::showEvent(QShowEvent *event)
{
    positionPanel();
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    QPainter p;
    QBrush brush2(Qt::red);
    p.begin(this);
    p.fillRect(QRect(0,0, width(), height()), brush2);
    p.end();
}
